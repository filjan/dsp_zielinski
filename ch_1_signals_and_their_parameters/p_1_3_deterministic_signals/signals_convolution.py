import numpy as np

class Convolution():
    def __init__(self, signal_x, signal_y):
        self.signal_x = signal_x
        self.signal_y = signal_y
        
        self.signal_x_values = self.signal_x.generate()
        self.signal_y_values = self.signal_y.generate()

    def estimate_convolution(self):
        # Prepare signal x - append trailing and leading zeros 
        signal_x = np.pad(self.signal_x_values, len(self.signal_y_values)-1, 'constant')
        # Prepare signal y - append leading zeros 
        signal_y = np.pad(np.flip(self.signal_y_values), (0,len(self.signal_x_values)-2+len(self.signal_y_values)))
        
        convolution_values = np.array([])
        
        for conv_iter in range(len(self.signal_y_values)-1 + len(self.signal_x_values)):
            
            rolled_signal_y = np.roll(signal_y, conv_iter)
            
            # Multiply corresponding values
            convolution_product = rolled_signal_y*signal_x
            # Sum values
            convolution_summation=sum(convolution_product)
            # Append 
            convolution_values = np.append(convolution_values, convolution_summation)              
                        
        return convolution_values              
            

if __name__ == "__main__":
    
    from ch_1_signals_and_their_parameters.p_1_2_signals_classification.signals_general_model import (
        SignalMock)
            
    domain = np.array((0,1,2,3,4,5))
    signal_x = SignalMock(domain, np.array([0,1,2,3,4,5,4,2,1,0]))
    signal_y = SignalMock(domain, np.array([9,9,9]))
    convo = Convolution(signal_x, signal_y)
    my_convo = convo.estimate_convolution()
    print(f"my_convo : {my_convo}")
    numpy_convo = np.convolve(signal_x.generate(), signal_y.generate())
    print(f"numpy_convo : {numpy_convo}")