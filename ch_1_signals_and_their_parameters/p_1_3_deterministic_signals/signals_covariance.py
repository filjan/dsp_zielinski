import numpy as np

from ch_1_signals_and_their_parameters.p_1_3_deterministic_signals.signals_correlation import (
    Correlation)

class Covariance(Correlation):
    def __init__(self, signal_x, signal_y):
        super().__init__(signal_x, signal_y)
        self.signal_x_values = self.signal_x_values - self.signal_x_values.mean()
        self.signal_y_values = self.signal_y_values - self.signal_y_values.mean()

if __name__ == "__main__":
    pass
