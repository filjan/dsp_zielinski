import numpy as np

class Correlation():
    def __init__(self, signal_x, signal_y):
        self.signal_x = signal_x
        self.signal_y = signal_y
        # Generated signal values
        self.signal_x_values = self.signal_x.generate()
        self.signal_y_values = np.conjugate(self.signal_y.generate())
        # Number of samples
        self.N = len(signal_x.domain)
        
    def estimate_nonstardardized(self):
        """
        Equivalent to np.correlate(x, y, 'full')
        """
        # Difference in length between signals
        length_difference = len(self.signal_x_values) - len(self.signal_y_values)
        # Prepare signal x - append trailing and leading zeros 
        signal_x = self.signal_x_values
        # Prepare signal y - append leading zeros 
        signal_y = np.pad(self.signal_y_values, (0, length_difference))
        # Container for correlation values  
        correlation_values = np.array([])

        for cor_iter in range(len(self.signal_x_values)):
            sub_signal_x = signal_x[:len(signal_x)-cor_iter]
            sub_signal_y = signal_y[cor_iter:]
            # Multiply corresponding values
            correlation_product = sub_signal_x*sub_signal_y
            # Sum values
            correlation_summation=sum(correlation_product)
            # Append 
            correlation_values = np.insert(correlation_values, 0, correlation_summation)

        for cor_iter in range(1,len(self.signal_x_values)):
            sub_signal_x = signal_x[cor_iter:]
            sub_signal_y = signal_y[:len(signal_x)-cor_iter]
            # Multiply corresponding values
            correlation_product = sub_signal_x*sub_signal_y
            # Sum values
            correlation_summation=sum(correlation_product)
            # Append 
            correlation_values = np.append(correlation_values, correlation_summation)
            
        return correlation_values[length_difference:]


    def estimate_standardized_nonbiased(self):
        # Nonstandardized 
        nonstandardized = self.estimate_nonstardardized()
        # Sequence of the same N values
        ns = np.ones(len(nonstandardized)) * self.N
        #
        ks = np.arange(-len(nonstandardized)//2+1, len(nonstandardized)//2+1, 1)
        # Standardization factor
        standardization_factor = 1/(ns - abs(ks))
        # Return altered values
        return standardization_factor * nonstandardized

    def estimate_standardized_biased(self):
        standardization_factor = (1/self.N)
        return standardization_factor* self.estimate_nonstardardized()

if __name__ == "__main__":
    from ch_1_signals_and_their_parameters.p_1_2_signals_classification.signals_general_model import (
        SignalMock)
            
    domain = np.array((0,1,2,3))
    signal_x = SignalMock(domain, np.array([0,1,2,3]))
    signal_y = SignalMock(domain, np.array([9,9,9,9]))
    corel = Correlation(signal_x, signal_y)
    my_corel = corel.estimate_nonstardardized()
    print(f"my_corel : {my_corel}")
    numpy_corel = np.correlate(signal_x.generate(), signal_y.generate(), "full")
    print(f"numpy_corel : {numpy_corel}")
    corel.estimate_standardized_nonbiased()
    corel.estimate_standardized_biased()
    
    