import numpy as np
import matplotlib.pyplot as plt

from ch_1_signals_and_their_parameters.p_1_2_signals_classification.signals_general_model import (
    SignalSinusoidal,SignalMultiSinusoidal, SignalLinear, SignalWaveRectangleBipolar)

from ch_1_signals_and_their_parameters.p_1_3_deterministic_signals.signals_correlation import (
    Correlation)

if __name__ == "__main__":
    domain = np.arange(0, 1, 0.001)

    signals = [    
        SignalSinusoidal(domain, amplitude=1, phase = 0, frequency=5.0),
        # SignalMultiSinusoidal(domain, amplitude=(1,0.5,0.25), phase=(0,0,0), frequency=(5,10,30))
        # SignalLinear(domain,interception=1, slope=1),
        # SignalWaveRectangleBipolar(domain, amplitude=1.0, period=2.0, tau=0.5)
    ]

    for signal_x in signals:
    
        signal_x_values = signal_x.generate()
        
        autocorrelation = Correlation(signal_x, signal_x)
        ac_nonstardardized = autocorrelation.estimate_nonstardardized()
        ac_stardardized_nonbiased = autocorrelation.estimate_standardized_nonbiased()
        
        ac_nonstardardized_numpy = np.correlate(signal_x_values, signal_x_values, "full")
        
        ac_stardardized_biased = autocorrelation.estimate_standardized_biased()
        
        fig, ax = plt.subplots(4,)
        
        ax[0].set_title("Signal")
        ax[0].plot(signal_x.generate())
        
        ax[1].set_title("Correlation nonstandardized")
        ax[1].plot(ac_nonstardardized, label="Filip's")
        ax[1].plot(ac_nonstardardized_numpy, label="numpy")    
        ax[1].legend()
        
        ax[2].set_title("Correlation standardized nonbiased")
        ax[2].plot(ac_stardardized_nonbiased)
    
        ax[3].set_title("Correlation standardized biased")
        ax[3].plot(ac_stardardized_biased)
        
        plt.tight_layout()
        plt.show()
    
