import numpy as np
import matplotlib.pyplot as plt

from ch_1_signals_and_their_parameters.p_1_2_signals_classification.signals_general_model import (
    SignalSinusoidal,
    SignalWaveRectangleBipolar,
    SignalWaveRectangleUnipolar,
)

from ch_1_signals_and_their_parameters.p_1_3_deterministic_signals.signals_parameters_models import (
    ParamMean,
    ParamEnergy,
    ParamPower,
    ParamRMS,
    ParamVariance,
    ParamCommonMoment,
    ParamCentralMoment,
    ParamNormalizedCommonMoment,
    ParamNormalizedCentralMoment,
    ParamCutCentroid,
    ParamQuadraticVarianceAroundCutCentroid,
    ParamMeanSquaredWidth,
)

if __name__ == "__main__":
    domain = np.arange(0, 8, 0.01)
    #
    signals = {
        # "sinusoidal" : SignalSinusoidal(domain, amplitude=1, phase = 0, frequency=3.0),
        "bipolar": SignalWaveRectangleBipolar(domain, amplitude=0.5, period=1, tau=0.5),
        "unipolar": SignalWaveRectangleUnipolar(domain, amplitude=1, period=1, tau=0.5),
    }
    #
    fig, ax = plt.subplots(len(signals))
    for num, (name, signal) in enumerate(signals.items()):
        mean = ParamMean(signal).estimate()
        energy = ParamEnergy(signal).estimate()
        power = ParamPower(signal).estimate()
        rms = ParamRMS(signal).estimate()
        variance = ParamVariance(signal).estimate()
        
        mom_com_1 = ParamCommonMoment(signal, 1).estimate()
        mom_com_2 = ParamCommonMoment(signal, 2).estimate()
        mom_com_3 = ParamCommonMoment(signal, 3).estimate()

        mom_cen_1 = ParamCentralMoment(signal, 1).estimate()
        mom_cen_2 = ParamCentralMoment(signal, 2).estimate()
        mom_cen_3 = ParamCentralMoment(signal, 3).estimate()

        mom_nor_com_1 = ParamNormalizedCommonMoment(signal, 1).estimate()
        mom_nor_com_2 = ParamNormalizedCommonMoment(signal, 2).estimate()
        mom_nor_com_3 = ParamNormalizedCommonMoment(signal, 3).estimate()

        mom_nor_cen_1 = ParamNormalizedCentralMoment(signal, 1).estimate()
        mom_nor_cen_2 = ParamNormalizedCentralMoment(signal, 2).estimate()
        mom_nor_cen_3 = ParamNormalizedCentralMoment(signal, 3).estimate()

        cut = ParamCutCentroid(signal).estimate()
        variance_cut = ParamQuadraticVarianceAroundCutCentroid(signal).estimate()
        width = ParamMeanSquaredWidth(signal).estimate()

        ax[num].plot(domain, signal.generate())
        ax[num].set_title(
            f"Signal {name} : mean : {mean:.2f} ; energy : {energy:.2f} ;"
            + f" power: {power:.2f} ; rms : {rms:.2f} ; variance : {variance:.2f} \n"
            + f"mom_com_1 : {mom_com_1:.2f} ; mom_com_2 : {mom_com_2:.2f} ; mom_com_3 : {mom_com_3:.2f} \n"
            + f"mom_cen_1 : {mom_cen_1:.2f} ; mom_cen_2 : {mom_cen_2:.2f} ; mom_cen_3 : {mom_cen_3:.2f} \n"
            + f"mom_nor_com_1 : {mom_nor_com_1:.2f} ; mom_nor_com_2 : {mom_nor_com_2:.2f} ; mom_nor_com_3 : {mom_nor_com_3:.2f} \n"
            + f"mom_nor_cen_1 : {mom_nor_cen_1:.2f} ; mom_nor_cen_2 : {mom_nor_cen_2:.2f} ; mom_nor_cen_3 : {mom_nor_cen_3:.2f} \n"
            + f"cut : {cut:.2f} ; variance_cut : {variance_cut:.2f} ; width : {width:.2f}"
        )

    fig.tight_layout()
    plt.show()
