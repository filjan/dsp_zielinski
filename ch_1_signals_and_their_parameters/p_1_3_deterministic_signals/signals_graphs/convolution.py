import numpy as np
import matplotlib.pyplot as plt

from ch_1_signals_and_their_parameters.p_1_2_signals_classification.signals_general_model import (
    SignalSinusoidal,SignalImpulseRectangle, SignalMultiSinusoidal)

from ch_1_signals_and_their_parameters.p_1_3_deterministic_signals.signals_convolution import (
    Convolution)

if __name__ == "__main__":
    domain_x = np.arange(-3, 3, 0.01)
    domain_y = np.arange(-1, 1, 0.01)
     
    signals = [    
        # (SignalSinusoidal(domain_x, amplitude=1, phase = 0, frequency=1.0), SignalImpulseRectangle(domain_y),)
        # (SignalSinusoidal(domain_x, amplitude=1, phase = 0, frequency=1.0), SignalImpulseRectangle(domain_y),)
        # (SignalSinusoidal(domain_x, amplitude=1, phase = 0, frequency=1.0), SignalImpulseRectangle(domain_y),)
        (SignalMultiSinusoidal(domain_x, amplitude=(1,1,1), phase=(0,0,0), frequency=(1,1,2)), SignalSinusoidal(domain_y, amplitude=1, phase = 0, frequency=1.0))
    ]

    for signal_x, signal_y in signals:
    
        convolution = Convolution(signal_x, signal_y)
        
        fig, ax = plt.subplots(6,sharex=True)
        
        ax[0].set_title("Signal x")
        ax[0].plot(signal_x.generate())
        
        ax[1].set_title("Signal y")
        ax[1].plot(signal_y.generate())
        
        ax[2].set_title("Signal x prepared")
        ax[2].plot(convolution.signal_x_values)

        ax[3].set_title("Signal y prepared")
        ax[3].plot(convolution.signal_y_values)

        ax[4].set_title("My Convolution")
        ax[4].plot(convolution.estimate_convolution())
        
        ax[5].set_title("Numpy convolution")
        ax[5].plot(np.convolve(signal_x.generate(), signal_y.generate(), "full"))
        
        plt.tight_layout()
        plt.show()
    
