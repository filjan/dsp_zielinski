import numpy as np
import matplotlib.pyplot as plt

from ch_1_signals_and_their_parameters.p_1_2_signals_classification.signals_general_model import (
    SignalSinusoidal,SignalMultiSinusoidal, SignalLinear, SignalWaveRectangleBipolar)

from ch_1_signals_and_their_parameters.p_1_3_deterministic_signals.signals_covariance import (
    Covariance)

if __name__ == "__main__":
    domain = np.arange(0, 6, 0.001)

    signals = [    
        (SignalSinusoidal(domain, amplitude=1, phase = 0, frequency=5.0), SignalLinear(domain,interception=1, slope=1),)
        # (SignalLinear(domain,interception=1, slope=1),SignalLinear(domain,interception=1, slope=2),)
        # (SignalWaveRectangleBipolar(domain, amplitude=1.0, period=2.0, tau=0.5), SignalLinear(domain,interception=1, slope=1))
    ]

    for signal_x, signal_y in signals:
    
        signal_x_values = signal_x.generate()
        signal_y_values = signal_y.generate()
        
        crosscorrelation = Covariance(signal_x, signal_y)
        ac_nonstardardized = crosscorrelation.estimate_nonstardardized()
        ac_stardardized_nonbiased = crosscorrelation.estimate_standardized_nonbiased()
        
        ac_nonstardardized_numpy = np.cov(signal_x_values, signal_y_values, "full")
        
        ac_stardardized_biased = crosscorrelation.estimate_standardized_biased()
        
        fig, ax = plt.subplots(5,)
        
        ax[0].set_title("Signal x")
        ax[0].plot(signal_x.generate())

        ax[1].set_title("Signal y")
        ax[1].plot(signal_y.generate())
        
        ax[2].set_title("Covariance nonstandardized")
        ax[2].plot(ac_nonstardardized, label="Filip's")
        ax[2].plot(ac_nonstardardized_numpy, label="numpy")    
        ax[2].legend()
        
        ax[3].set_title("Covariance standardized nonbiased")
        ax[3].plot(ac_stardardized_nonbiased)
    
        ax[4].set_title("Covariance standardized biased")
        ax[4].plot(ac_stardardized_biased)
        
        plt.tight_layout()
        plt.show()
    
