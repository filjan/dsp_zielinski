import numpy as np
import matplotlib.pyplot as plt

from ch_1_signals_and_their_parameters.p_1_3_deterministic_signals.signals_FT import (
    FourierTransform)

from ch_1_signals_and_their_parameters.p_1_2_signals_classification.signals_general_model import (
    SignalSinusoidal, SignalMultiSinusoidal)

if __name__ == "__main__":
    
    domain = np.arange(0, 1, 0.001)
    fig, ax = plt.subplots(4,2)

    sig_sinus = SignalSinusoidal(domain, amplitude=1, phase=0, frequency=5)
    my_ft_sinus = FourierTransform(sig_sinus)
    my_ft_sinus_trans = my_ft_sinus.transform_normalized()        

    ax[0][0].plot(sig_sinus.generate())
    ax[0][0].set_title("Signal")
    
    ax[1][0].plot(my_ft_sinus_trans.real)
    ax[1][0].set_title("DFT - real")
    
    ax[2][0].plot(my_ft_sinus_trans.imag)
    ax[2][0].set_title("DFT - imag")
    
    ax[3][0].plot(abs(my_ft_sinus_trans), label="my fft")
    ax[3][0].plot(abs(np.fft.fft(sig_sinus.generate(),norm="forward")), label="np fft")
    ax[3][0].set_title("DFT - abs")
    ax[3][0].legend()

    sig_multisinus = SignalMultiSinusoidal(domain, amplitude=(1,0.5,0.25), phase=(0,0,0), frequency=(5,10,30))

    my_ft_multisinus = FourierTransform(sig_multisinus)
    my_ft_multisinus_trans = my_ft_multisinus.transform_normalized()        
    
    ax[0][1].plot(sig_multisinus.generate())
    ax[0][1].set_title("Signal")
    
    ax[1][1].plot(my_ft_multisinus_trans.real)
    ax[1][1].set_title("DFT - real")
    
    ax[2][1].plot(my_ft_multisinus_trans.imag)
    ax[2][1].set_title("DFT - imag")
    
    ax[3][1].plot(abs(my_ft_multisinus_trans), label="my fft")
    ax[3][1].plot(abs(np.fft.fft(sig_multisinus.generate(),norm="forward")), label="np fft")
    ax[3][1].set_title("DFT - abs")
    ax[3][1].legend()

    plt.tight_layout()
    plt.show()

