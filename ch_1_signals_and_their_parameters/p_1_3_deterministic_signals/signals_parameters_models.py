"""
Module contains classes used to estimate various type of parameters of
deterministic signals
"""

from abc import ABC, abstractmethod
import numpy as np

from ch_1_signals_and_their_parameters.p_1_2_signals_classification.signals_general_model import (
    Signal,
)


class Parameter(ABC):
    @abstractmethod
    def __init__(self, signal: Signal):
        pass

    @abstractmethod
    def estimate(self):
        pass


class ParamMean(Parameter):
    def __init__(self, signal: Signal):
        self.signal = signal
        self.signal_values = signal.generate()

    def estimate(self):
        return sum(self.signal_values) / len(self.signal_values)


class ParamEnergy(Parameter):
    def __init__(self, signal: Signal):
        self.signal = signal
        self.signal_values = signal.generate()

    def estimate(self):
        return abs(sum(self.signal_values ** 2))


class ParamPower(Parameter):
    def __init__(self, signal: Signal):
        self.signal = signal
        self.signal_values = signal.generate()

    def estimate(self):
        return abs(ParamEnergy(self.signal).estimate() / len(self.signal_values))


class ParamRMS(Parameter):
    """
    RMS = Root Mean Square 
    """

    def __init__(self, signal: Signal):
        self.signal = signal
        self.signal_values = signal.generate()

    def estimate(self):
        return ParamPower(self.signal).estimate() ** (0.5)


class ParamVariance(Parameter):
    """
    """

    def __init__(self, signal: Signal):
        self.signal = signal
        self.signal_values = signal.generate()

    def estimate(self):
        return sum((self.signal_values - ParamMean(self.signal).estimate()) ** 2) / len(
            self.signal_values
        )


class ParamCommonMoment(Parameter):
    def __init__(self, signal: Signal, order: int):
        self.order = order
        self.signal = signal
        self.signal_values = signal.generate()

    def estimate(self):
        return sum(self.signal.domain ** self.order * self.signal_values)


class ParamCentralMoment(Parameter):
    def __init__(self, signal: Signal, order: int):
        self.order = order
        self.signal = signal
        self.signal_values = signal.generate()

    def estimate(self):
        return sum(
            (
                (self.signal.domain - ParamCommonMoment(self.signal, 1).estimate())
                ** self.order
            )
            * self.signal_values
        )


class ParamNormalizedCommonMoment(Parameter):
    def __init__(self, signal: Signal, order: int):
        self.order = order
        self.signal = signal
        self.signal_values = signal.generate()

    def estimate(self):
        return ParamCommonMoment(self.signal, self.order).estimate() / sum(
            self.signal_values
        )


class ParamNormalizedCentralMoment(Parameter):
    def __init__(self, signal: Signal, order: int):
        self.order = order
        self.signal = signal
        self.signal_values = signal.generate()

    def estimate(self):
        return ParamCentralMoment(self.signal, self.order).estimate() / sum(
            self.signal_values
        )


class ParamCutCentroid(Parameter):
    """
    Odcieta srodka ciezkosci kwadratu sygnalu - 
    """

    def __init__(self, signal: Signal):
        self.signal = signal
        self.signal_values = signal.generate()

    def estimate(self):
        nominator = sum(self.signal.domain * self.signal_values ** 2)
        denominator = ParamEnergy(self.signal).estimate()
        return nominator / denominator


class ParamQuadraticVarianceAroundCutCentroid(Parameter):
    """
    Wariancja kwadratu sygnalu wokol odcietej srodka ciezkosci kwadratu sygnalu 
    """

    def __init__(self, signal: Signal):
        self.signal = signal
        self.signal_values = signal.generate()

    def estimate(self):
        nominator = sum(
            (self.signal.domain - ParamCutCentroid(self.signal).estimate()) ** 2
            * self.signal_values ** 2
        )
        denominator = ParamEnergy(self.signal).estimate()
        return nominator / denominator


class ParamMeanSquaredWidth(Parameter):
    """
    Szerokosc sredniokwadratowa
    """

    def __init__(self, signal: Signal):
        self.signal = signal
        self.signal_values = signal.generate()

    def estimate(self):
        return ParamQuadraticVarianceAroundCutCentroid(self.signal).estimate() ** (0.5)
