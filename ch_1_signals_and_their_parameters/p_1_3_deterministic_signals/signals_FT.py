import numpy as np

class FourierTransform:
    def __init__(self, signal):
        self.signal = signal
        self.signal_values = self.signal.generate()
        
    def transform(self):

        # Array of k indices of frequencies
        ks = np.arange(0,len(self.signal_values),1)        
        # Array of n indices of samples        
        ns = np.arange(0,len(self.signal_values),1)
        # Container for values of each frequency
        transformation_values = np.array([])
        # Array of tranformation values
        transformation_formulae = map(lambda k: sum(self.signal_values * np.exp(-1j*2*np.pi*(k/len(self.signal_values))*ns)), ks)
        transformation_values = np.fromiter(transformation_formulae, dtype=complex)
        return transformation_values

    def transform_normalized(self):
        transformation_values = self.transform()
        transformation_values /= len(self.signal_values)
        return transformation_values


    def transform_inverted(self, signal_values_fourier):
        ks = np.arange(0, len(signal_values_fourier), 1)
        ns = np.arange(0, len(signal_values_fourier), 1)
        # Array of tranformation values
        container = []
        for n in ns:    
            part_sum=0
            for k in ks:    
                part_sum += signal_values_fourier[k]*np.exp(1j*2*np.pi*(k/len(signal_values_fourier))*n)
            container.append(part_sum)
        return container
        
        

if __name__ == "__main__":
    
    from ch_1_signals_and_their_parameters.p_1_2_signals_classification.signals_general_model import (
        SignalMultiSinusoidal)
    
    domain = np.arange(0,4,0.1)
    sig = SignalMultiSinusoidal(domain, amplitude=(1, 2), 
                                phase=(3, 0), frequency=(0.2, 0.5))
    
    sig_values = sig.generate()
    np_fft_sig = np.fft.fft(sig_values)
    np_fft_fre = np.fft.fftfreq(len(sig_values))
    
    fourier = FourierTransform(sig)
    my_ft_sig = fourier.transform()
    
    
    print("FT:")    
    assert len(np_fft_sig) == len(my_ft_sig), "Different length!"
    for my_val, np_val in zip(my_ft_sig, np_fft_sig):
        print(f"my_val - np_val : {my_val - np_val:.5f}")
        assert round(abs(my_val - np_val),3) == 0.0

    print("Normalized FT:")    
    np_fft_sig_norm = np.fft.fft(sig_values, norm="forward")
    my_ft_sig_norm = fourier.transform_normalized()
    assert len(np_fft_sig_norm) == len(my_ft_sig_norm), "Different length!"
    for my_val, np_val in zip(my_ft_sig_norm, np_fft_sig_norm):
        print(f"my_val - np_val : {my_val - np_val:.5f}")
        assert round(abs(my_val - np_val),3) == 0.0

    print("IFT:")
    my_inv_ft_sig = fourier.transform_inverted(my_ft_sig)
    np_inv_fft_sig = np.fft.ifft(my_ft_sig, norm="forward")
    assert len(np_fft_sig_norm) == len(my_ft_sig_norm), "Different length!"
    for my_val, np_val in zip(my_inv_ft_sig, np_inv_fft_sig):
        print(f"my_val - np_val : {my_val - np_val:.5f}")
        assert round(abs(my_val - np_val),3) == 0.0    
    