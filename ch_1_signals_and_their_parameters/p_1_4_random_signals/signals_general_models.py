from functools import partial

import numpy as np

from ch_1_signals_and_their_parameters.p_1_2_signals_classification.signals_general_model import (
    Signal)

def prepare_normal_noise(x_mean, x_std):
    def add_noise(signal:Signal):
        pure_signal = signal.shape(signal.domain)
        noise = np.random.normal(x_mean, x_std, len(signal.domain))
        return pure_signal + noise
    return add_noise

def prepare_square_noise(low, high):
    def add_noise(signal:Signal):
        pure_signal = signal.shape(signal.domain)
        noise = np.random.uniform(low, high, len(signal.domain))
        return pure_signal + noise
    return add_noise

def overwrite_generate(signal:Signal, noise_function):
    signal.generate = partial(noise_function, signal)
    

if __name__ == "__main__":
    
    
    import matplotlib.pyplot as plt

    from ch_1_signals_and_their_parameters.p_1_2_signals_classification.signals_general_model import (
        SignalLinear)

    domain = np.arange(0, 7, 0.01)
    signal_lin = SignalLinear(domain, 0, 1)
    
    fig, ax = plt.subplots(2,2)
    ax[0][0].set_title("Pure signal")
    ax[0][0].plot(domain, signal_lin.generate())
    
    ax[1][0].set_title("Normal noise added")
    overwrite_generate(signal_lin, prepare_normal_noise(x_mean=0.0, x_std=0.2))
    ax[1][0].plot(domain, signal_lin.generate())
    
    ax[1][1].set_title("Square noise added")
    overwrite_generate(signal_lin, prepare_square_noise(0.0, 1.0))
    ax[1][1].plot(domain, signal_lin.generate())
    
    fig.tight_layout()
    plt.show()