import numpy as np
import matplotlib.pyplot as plt

from ch_1_signals_and_their_parameters.p_1_2_signals_classification.signals_general_model import (
    SignalLinear)

from ch_1_signals_and_their_parameters.p_1_4_random_signals.signals_general_models import (
    add_noise_to_signal, noise_normal, noise_square)

domain = np.arange(0, 1, 0.001)
signal_lin = SignalLinear(domain, 0, 0)
signal_pure = signal_lin.generate()

add_noise_to_signal(signal_lin, noise_normal(x_mean=0.0, x_std=0.2))
signal_noised_normal = signal_lin.generate()

add_noise_to_signal(signal_lin, noise_square(-1.0, 1.0))
signal_noised_square = signal_lin.generate()


fig, ax = plt.subplots(4,2)
ax[0][0].set_title("Pure signal")
ax[0][0].plot(domain, signal_pure)

ax[1][0].set_title("Normal noise added")
ax[1][0].plot(domain, signal_noised_normal)

ax[2][0].set_title("Probability Function Density ")
ax[2][0].hist(signal_noised_normal, density=True)
ax[3][0].set_title("Cumulative Distribution Function")
ax[3][0].hist(signal_noised_normal, density=True, cumulative=True)

ax[1][1].set_title("Square noise added")
ax[1][1].plot(domain, signal_noised_square)
ax[2][1].hist(signal_noised_square, density=True)
ax[3][1].hist(signal_noised_square, density=True, cumulative=True)

fig.tight_layout()
plt.show()