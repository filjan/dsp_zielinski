"""
Module contains classes used to generate various types of signals.
"""

from abc import ABC, abstractmethod
import numpy as np
import numpy.typing as npt
from scipy.integrate import cumtrapz

class Signal(ABC):
    """
    Abstract Signal class
    """
    @abstractmethod
    def __init__(self, domain):
        pass


    @abstractmethod
    def shape(self, argument):
        """
        Each Signal class has to comprise shape function. The method takes
        np.array or a signle value of domain and returns np.array of generated signal
        """

    @abstractmethod
    def generate(self):
        """
        generate method calls shape function
        """

class SignalLinear(Signal):
    """
    Simple linear signal
    """

    def __init__(self, domain, interception=0, slope=0 ):
        self.domain = domain
        self.interception = interception
        self.slope = slope

    def shape(self, argument):
        shape_core = lambda x:  self.slope * x + self.interception
        return shape_core(argument)

    def generate(self):
        return self.shape(self.domain)


class SignalSinusoidal(Signal):
    """
    Simple single sinusoidal signal with given amplitude, phase and frequency
    """

    def __init__(self, domain, amplitude=1, phase=0, frequency=1):
        self.domain = domain
        self.amplitude = amplitude
        self.phase = phase
        self.frequency = frequency

    def shape(self, argument):
        shape_core = lambda x: self.amplitude * np.sin(
            2 * np.pi * self.frequency * x + self.phase
        )
        return shape_core(argument)

    def generate(self):
        return self.shape(self.domain)


class SignalBisinusoidal(Signal):
    def __init__(self, domain, amplitude=(1, 2), phase=(0, 0), frequency=(1, 2)):
        self.domain = domain
        self.sine_1 = SignalSinusoidal(
            domain, amplitude=amplitude[0], phase=phase[0], frequency=frequency[0]
        )
        self.sine_2 = SignalSinusoidal(
            domain, amplitude=amplitude[1], phase=phase[1], frequency=frequency[1]
        )

    def shape(self, argument):
        shape_core = lambda x: self.sine_1.shape(x) + self.sine_2.shape(x)
        return shape_core(argument)

    def generate(self):
        return self.sine_1.generate() + self.sine_2.generate()

class SignalMultiSinusoidal(Signal):
    def __init__(self, domain, amplitude=(1, 2), phase=(0, 0), frequency=(1, 2)):
        self.domain = domain
        self.amplitude = amplitude
        self.phase = phase
        self.frequency = frequency
        if len(amplitude) != len(phase) or len(phase) != len(frequency):
            raise Exception("Each tuple must be equally length")
        if not isinstance(amplitude, tuple) or not isinstance(phase, tuple) or not isinstance(frequency, tuple):
            raise Exception("amplitude, phase and freqency input must be of tuple type")            
        self.sinusoidal_signals = []
        for amp, pha, fre in zip(self.amplitude, self.phase, self.frequency):
            self.sinusoidal_signals.append(SignalSinusoidal(self.domain, amplitude=amp, phase=pha, frequency=fre))

    def shape(self, argument):
        shape_core = lambda x: sum([signal.shape(x) for signal in self.sinusoidal_signals])
        return shape_core(argument)

    def generate(self):
        return sum([signal.generate() for signal in self.sinusoidal_signals])

class SignalImpulseRectangle(Signal):
    def __init__(self, domain):
        self.domain = domain

    @staticmethod
    def shape(argument):

        shape_core = lambda x: np.where(((x < -0.5) | (x > 0.5)), 0.0, 1.0)
        return shape_core(argument)

    def generate(self):
        return self.shape(self.domain)


class SignalImpulseTriangle(Signal):
    def __init__(self, domain):
        self.domain = domain

    @staticmethod
    def shape(argument):

        shape_core = lambda x: 1.0 - abs(x)

        shape_ranges = lambda x: np.where(((x < -1.0) | (x > 1.0)), 0.0, shape_core(x))

        return shape_ranges(argument)

    def generate(self):
        return self.shape(self.domain)


class SignalImpulseCosine(Signal):
    def __init__(self, domain, period=1):
        self.domain = domain
        self.period = period
        self.frequency = 1 / period
        self.omega_0 = 2 * np.pi * self.frequency

    def shape(self, argument):

        shape_core = lambda x: np.cos(self.omega_0 * x) * SignalImpulseRectangle.shape(
            x / (self.period / 2)
        )

        shape_ranges = lambda x: np.where(
            ((x < -1 * self.period / 4) | (x > self.period / 4)), 0.0, shape_core(x),
        )

        return shape_ranges(argument)

    def generate(self):
        return self.shape(self.domain)


class SignalImpulseExponential(Signal):
    def __init__(self, domain, period=1, alfa=1):
        self.domain = domain
        self.period = period
        self.alfa = alfa
        if alfa <= 0:
            raise Exception("Alfa must be positive!")

    def shape(self, argument):

        shape_core = lambda x: np.e ** (
            -1 * self.alfa * x
        ) * SignalImpulseRectangle.shape((x - (self.period / 2)) / (self.period))

        shape_ranges = lambda x: np.where(
            ((x < 0) | (x > self.period)), 0.0, shape_core(x)
        )

        return shape_ranges(argument)

    def generate(self):
        return self.shape(self.domain)


class SignalExponentialDecreasing(Signal):
    def __init__(self, domain, amplitude=1, alfa=1):
        self.domain = domain
        self.amplitude = amplitude
        self.alfa = alfa
        if alfa <= 0:
            raise Exception("Alfa must be positive number")

    def shape(self, argument):

        shape_core = lambda x: self.amplitude * np.e ** (-1 * self.alfa * x)

        shape_ranges = lambda x: np.where((x < 0), 0.0, shape_core(x))

        return shape_ranges(argument)

    def generate(self):
        return self.shape(self.domain)


class SignalSinusoidalExponentiallyDecreasing(Signal):
    def __init__(self, domain, amplitude=1, alfa=1, frequency=1):
        self.domain = domain
        self.amplitude = amplitude
        self.alfa = alfa
        if alfa <= 0:
            raise Exception("Alfa must be positive number")
        self.frequency = frequency

    def shape(self, argument):

        shape_core = (
            lambda x: self.amplitude
            * np.e ** (-1 * self.alfa * x)
            * np.sin(2 * np.pi * self.frequency * x)
        )

        shape_ranges = lambda x: np.where((x < 0), 0.0, shape_core(x))

        return shape_ranges(argument)

    def generate(self):
        return self.shape(self.domain)


class SignalSinc(Signal):
    def __init__(self, domain):
        self.domain = domain

    def shape(self, argument):

        shape_core_ranges = lambda x: np.piecewise(
            x,
            [x == 0, x != 0],
            [lambda x: 1, lambda x: np.sin(np.pi * x) / (np.pi * x)],
        )
        return shape_core_ranges(argument)

    def generate(self):
        return self.shape(self.domain)


class SignalGaussian(Signal):
    def __init__(self, domain):
        self.domain = domain

    @staticmethod
    def shape(argument):

        shape_core = lambda x: np.exp(-1 * np.pi * x ** 2)

        return shape_core(argument)

    def generate(self):
        return self.shape(self.domain)


class SignalStep(Signal):
    def __init__(self, domain):
        self.domain = domain

    @staticmethod
    def shape(argument):

        shape_core = 1.0

        shape_ranges = lambda x: np.where((x < 0.0), 0.0, shape_core)

        return shape_ranges(argument)

    def generate(self):
        return self.shape(self.domain)


class SignalExponentiallyIncreasing(Signal):
    def __init__(self, domain, alfa=1):
        self.domain = domain
        self.alfa = alfa
        if alfa <= 0:
            raise Exception("Alfa must be positive number")
        self.step_signal = SignalStep(self.domain)

    def shape(self, argument):

        shape_core = lambda x: (
            1 - np.e ** (-1 * self.alfa * x)
        ) * self.step_signal.shape(argument)

        return shape_core(argument)

    def generate(self):
        return self.shape(self.domain)


class SignalSignum(Signal):
    def __init__(self, domain):
        self.domain = domain

    @staticmethod
    def shape(argument):

        shape_core_ranges = lambda x: np.piecewise(
            x, [x < 0, x == 0, x > 0], [lambda x: -1, lambda x: 0, lambda x: 1]
        )

        return shape_core_ranges(argument)

    def generate(self):
        return self.shape(self.domain)


class SignalIntegralSinc(Signal):
    def __init__(self, domain):
        self.domain = domain
        self.signal_sinc = SignalSinc(domain)

    def shape(self, argument):
        # TODO - dobrze to opisac
        if isinstance(argument,float):
            integration_domain = self.domain[:np.where(self.domain == argument)[0][0]]
        else:
            integration_domain = self.domain
            
        shape_core = lambda x: cumtrapz(self.signal_sinc.shape(x), x, initial=self.signal_sinc.shape(x)[0])
        return shape_core(integration_domain)

    def generate(self):
        return self.shape(self.domain)


class SignalWaveRectangleBipolar(Signal):
    def __init__(self, domain, amplitude=1, period=1, tau=0.5):
        self.domain = domain
        self.amplitude = amplitude
        self.period = period
        self.tau = tau
        if tau >= period:
            raise Exception("Tau must be lower than period")

    def shape(self, argument):
        shape_core = lambda x: np.piecewise(
            x,
            [
                x % self.period < self.tau,
                ((x % self.period == 0.0) | (x % self.period == self.tau)),
                x % self.period > self.tau,
            ],
            [lambda x: self.amplitude, lambda x: 0, lambda x: -1 * self.amplitude],
        )

        return shape_core(argument)

    def generate(self):
        return self.shape(self.domain)


class SignalWaveRectangleUnipolar(Signal):
    def __init__(self, domain, amplitude=1, period=1, tau=0.5):
        self.domain = domain
        self.amplitude = amplitude
        self.period = period
        self.tau = tau
        if tau >= period:
            raise Exception("Tau must be lower than period")

    def shape(self, argument):

        shape_core = lambda x: np.piecewise(
            x,
            [
                x % self.period < self.tau,
                ((x % self.period == 0.0) | (x % self.period == self.tau)),
                x % self.period > self.tau,
            ],
            [lambda x: self.amplitude, lambda x: self.amplitude / 2, lambda x: 0],
        )

        argument_modified = argument + self.tau / 2

        return shape_core(argument_modified)

    def generate(self):
        return self.shape(self.domain)


class SignalSinusoidalAmplitudeModulated(Signal):
    def __init__(self, domain: npt.ArrayLike, carrier_signal: SignalSinusoidal, modulating_signal: Signal, k_a: float):
        self.domain = domain
        self.carrier_signal = carrier_signal
        self.modulating_signal = modulating_signal
        # Modulation depth
        self.k_a = k_a
        if k_a < 0 or k_a > 1:
            raise Exception("Modulation depth must be within 0 to 1")

    def shape(self, argument):
        shape_core = lambda x: self.carrier_signal.shape(x)*(1+self.k_a*self.modulating_signal.shape(x))
        return shape_core(argument)
        
    def generate(self):
        return self.shape(self.domain)


class SignalSinusoidalPhaseModulated(Signal):
    def __init__(self, domain: npt.ArrayLike, carrier_signal: SignalSinusoidal, modulating_signal:Signal, k_fi:float):
        self.domain = domain
        self.carrier_signal = carrier_signal
        self.modulating_signal = modulating_signal
        # Modulation depth
        self.k_fi = k_fi
        if k_fi < 0 or k_fi > 1:
            raise Exception("Modulation depth must be within 0 to 1")

    def shape(self, argument):
        
        shape_core = lambda x: self.carrier_signal.amplitude * np.sin(
            2 * np.pi * self.carrier_signal.frequency * x
            + self.carrier_signal.phase
            + self.k_fi * self.modulating_signal.shape(x)
        )
        return shape_core(argument)

    def generate(self):
        return self.shape(self.domain)


class SignalSinusoidalFrequencyModulated(Signal):
    def __init__(self, domain: npt.ArrayLike, carrier_signal: SignalSinusoidal, modulating_signal:Signal, k_f:float):
        self.domain = domain
        self.carrier_signal = carrier_signal
        self.modulating_signal = modulating_signal
        # Modulation depth
        self.k_f = k_f
        if k_f < 0 or k_f > 1:
            raise Exception("Modulation depth must be within 0 to 1")

    def shape(self, argument):
        
        if isinstance(argument,float):
            integration_domain = self.domain[:np.where(self.domain == argument)[0][0]]
        else:
            integration_domain = self.domain
        
        shape_integral = cumtrapz(self.modulating_signal.shape(integration_domain), integration_domain, initial=float(self.modulating_signal.shape(self.domain[0])))
        
        shape_core_1 = lambda x: 2 * np.pi * self.carrier_signal.frequency * x
        
        shape_core_2 = self.carrier_signal.amplitude * np.cos(
            shape_core_1(argument) + self.carrier_signal.phase + self.k_f * shape_integral)
        
        return shape_core_2

    def generate(self):
        return self.shape(self.domain)


class SignalComplexExponential(Signal):
    def __init__(self, domain):
        self.domain = domain

    def shape(self,argument):
        shape_core = lambda x: SignalSinusoidal(self.domain, phase = np.pi/2).shape(x) + 1j*SignalSinusoidal(self.domain).shape(x)
        return shape_core(argument)
    
    def generate(self):
        return self.shape(self.domain)

class SignalAnalytic(Signal):
    def __init__(self, domain):
        self.domain = domain

    def shape(self,argument):
        pass
        #TODO
        
    def generate(self):
        pass
        #TODO

class SignalMock(Signal):
    def __init__(self, domain, values):
        self.domain = domain
        self.values = values
    def shape(self):
        pass
    def generate(self):
        return self.values

if __name__ == "__main__":
    example_domain = np.array([1, 2, 3])
    SignalLinear(example_domain).generate()
    SignalSinusoidal(example_domain).generate()
    SignalBisinusoidal(example_domain).generate()
    SignalImpulseRectangle(example_domain).generate()
    SignalImpulseTriangle(example_domain).generate()
    SignalImpulseCosine(example_domain).generate()
    SignalImpulseExponential(example_domain).generate()
    SignalExponentialDecreasing(example_domain).generate()
    SignalSinusoidalExponentiallyDecreasing(example_domain).generate()
    SignalSinc(example_domain).generate()
    SignalGaussian(example_domain).generate()
    SignalStep(example_domain).generate()
    SignalExponentiallyIncreasing(example_domain).generate()
    SignalSignum(example_domain).generate()
    SignalIntegralSinc(example_domain).generate()
    SignalWaveRectangleBipolar(example_domain).generate()
    SignalWaveRectangleUnipolar(example_domain).generate()
    
    SignalSinusoidalAmplitudeModulated(example_domain, SignalSinusoidal(example_domain), SignalSinusoidal(example_domain),0.5 ).generate()
    SignalSinusoidalPhaseModulated(example_domain, SignalSinusoidal(example_domain), SignalSinusoidal(example_domain),0.5 ).generate()
    SignalSinusoidalFrequencyModulated(example_domain, SignalSinusoidal(example_domain), SignalSinusoidal(example_domain),0.5 ).generate()
    
    SignalComplexExponential(example_domain).generate()
    
