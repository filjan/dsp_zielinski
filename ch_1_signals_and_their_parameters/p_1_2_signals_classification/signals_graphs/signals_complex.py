import numpy as np
import matplotlib.pyplot as plt

from ch_1_signals_and_their_parameters.p_1_2_signals_classification.signals_general_model import (
    SignalComplexExponential)

if  __name__ == "__main__":
    
    domain = np.arange(0, 7, 0.001)
    
    signal_complex_expo = SignalComplexExponential(domain)
    
    fig, ax = plt.subplots(2)
    ax[0].plot(domain, signal_complex_expo.generate().imag, label="imag")
    ax[0].plot(domain, signal_complex_expo.generate().real, label="real")
    ax[0].legend()
    ax[0].set_title("Complex Exponential (Euler)")
    ax[1].set_title("Analytic signal") #TODO
    fig.tight_layout()
    plt.show()
    
    