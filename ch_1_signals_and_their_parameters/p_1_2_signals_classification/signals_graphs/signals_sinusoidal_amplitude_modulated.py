import numpy as np
import matplotlib.pyplot as plt

from ch_1_signals_and_their_parameters.p_1_2_signals_classification.signals_general_model import SignalSinusoidal, SignalSinusoidalAmplitudeModulated

if  __name__ == "__main__":
    domain = np.arange(0, int(2*np.pi), 0.002)
    
    signal_sinusoidal_1 = SignalSinusoidal(domain, amplitude=1, phase = 0, frequency=10.0)
    signal_sinusoidal_2 = SignalSinusoidal(domain, amplitude=1, phase = 0, frequency=1.0)
    signal_amp_mod = SignalSinusoidalAmplitudeModulated(domain,signal_sinusoidal_1, signal_sinusoidal_2, k_a=0.3)
    
    fig, ax = plt.subplots(3, )
    ax[0].plot(domain, signal_sinusoidal_1.generate())
    ax[0].set_title("Carrier")
    ax[1].plot(domain, signal_sinusoidal_2.generate())
    ax[1].set_title("Modulating wave (message signal)")
    ax[2].plot(domain, signal_amp_mod.generate())
    ax[2].set_title("Amplitude modulation")
    
    fig.tight_layout()
    plt.show()
    
    
    
    
    
    