import numpy as np
import matplotlib.pyplot as plt

from ch_1_signals_and_their_parameters.p_1_2_signals_classification.signals_general_model import (
    SignalSinusoidal, SignalBisinusoidal, SignalWaveRectangleBipolar, 
    SignalWaveRectangleUnipolar, SignalMultiSinusoidal)

if  __name__ == "__main__":
    domain = np.arange(0, int(2*np.pi), 0.001)
    signal_sinusoidal = SignalSinusoidal(domain,phase=-0.1)
    signal_bisinusoidal_periodic = SignalBisinusoidal(domain, (1, 2))
    signal_trisinusoidal_periodic = SignalMultiSinusoidal(domain, amplitude=(1,0.5,0.25),
                                                          phase=(0,0,0), frequency=(5,10,30))
    
    signal_rect_bipolar = SignalWaveRectangleBipolar(domain, amplitude=1, period=2, tau=1.5)
    signal_rect_unipolar = SignalWaveRectangleUnipolar(domain, amplitude=1, period=3, tau=2.0)
    
    fig, ax = plt.subplots(5)
    ax[0].plot(domain, signal_sinusoidal.generate())
    ax[1].plot(domain, signal_bisinusoidal_periodic.generate())
    ax[2].plot(domain, signal_rect_bipolar.generate(), marker = "o")
    ax[3].plot(domain, signal_rect_unipolar.generate(), marker = "o")
    ax[4].plot(domain, signal_trisinusoidal_periodic.generate())
    [axes.grid() for axes in ax]
    plt.show()