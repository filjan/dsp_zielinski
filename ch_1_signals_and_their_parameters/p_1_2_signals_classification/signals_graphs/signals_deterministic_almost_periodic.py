import numpy as np
import matplotlib.pyplot as plt

from ch_1_signals_and_their_parameters.p_1_2_signals_classification.signals_general_model import (
    SignalBisinusoidal,
)


if __name__ == "__main__":

    domain = np.arange(0, 2 * np.pi * 2, np.pi / 100)
    # Signal is almost periodic when ratio of frequencies of essential sines
    # is immeasureable
    signal = SignalBisinusoidal(domain, frequency=(np.e / 2, 1.0))
    plt.plot(domain, signal.generate())
    plt.grid()
