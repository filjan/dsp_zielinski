from matplotlib import pyplot as plt
import numpy as np

from ch_1_signals_and_their_parameters.p_1_2_signals_classification.signals_general_model import SignalImpulseRectangle, SignalImpulseTriangle, SignalImpulseCosine, SignalImpulseExponential


if  __name__ == "__main__":

    domain = np.linspace(-2, 2, 1000)
    signal_rectangle = SignalImpulseRectangle(domain)
    signal_triangle = SignalImpulseTriangle(domain)
    signal_cosine = SignalImpulseCosine(domain, 1)
    signal_expo = SignalImpulseExponential(domain, 1, 1)
    
    fig, ax = plt.subplots(4)
    ax[0].plot(domain, signal_rectangle.generate())
    ax[1].plot(domain, signal_triangle.generate())
    ax[2].plot(domain, signal_cosine.generate())
    ax[3].plot(domain, signal_expo.generate())
    
    plt.show()    