from matplotlib import pyplot as plt
import numpy as np

from ch_1_signals_and_their_parameters.p_1_2_signals_classification.signals_general_model import SignalStep, SignalExponentiallyIncreasing, SignalSignum, SignalIntegralSinc


if  __name__ == "__main__":

    domain = np.linspace(-20, 20, 399)
    signal_step = SignalStep(domain)
    signal_expo_incr = SignalExponentiallyIncreasing(domain, alfa=4)
    signal_signum = SignalSignum(domain)
    signal_integral_sinc = SignalIntegralSinc(domain)
    
    fig, ax = plt.subplots(4)
    ax[0].plot(domain, signal_step.generate())
    ax[1].plot(domain, signal_expo_incr.generate())
    ax[2].plot(domain, signal_signum.generate(), marker="o")
    ax[3].plot(domain, signal_integral_sinc.generate())
    [axes.grid() for axes in ax]
    [axes.set_xticks(np.arange(domain[0], domain[-1]+1, 1)) for axes in ax]
    plt.show()    