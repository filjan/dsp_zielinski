import numpy as np
import matplotlib.pyplot as plt

from ch_1_signals_and_their_parameters.p_1_2_signals_classification.signals_general_model import (
    SignalLinear, SignalSinusoidal, SignalSinusoidalPhaseModulated, 
    SignalSinusoidalFrequencyModulated, SignalExponentiallyIncreasing,
    SignalWaveRectangleBipolar, SignalWaveRectangleUnipolar)

if  __name__ == "__main__":
    domain = np.arange(0, 2*np.pi, 0.001)
    
    signal_sinusoidal_1 = SignalSinusoidal(domain, amplitude=1, phase = 0, frequency=1.0)
    signal_linear = SignalLinear(domain, slope=5, )
    
    signal_phi_mod = SignalSinusoidalPhaseModulated(domain, signal_sinusoidal_1, signal_linear, k_fi=1.0)
    signal_fre_mod = SignalSinusoidalFrequencyModulated(domain, signal_sinusoidal_1, signal_linear, k_f=1.0)
    
    fig, ax = plt.subplots(4,2 )
    ax[0][0].plot(domain, signal_sinusoidal_1.generate())
    ax[0][0].set_title("Carrier")
    ax[1][0].plot(domain, signal_linear.generate())
    ax[1][0].set_title("Modulating wave")
    ax[2][0].plot(domain, signal_phi_mod.generate())
    ax[2][0].set_title("Phase modulation")
    ax[3][0].plot(domain, signal_fre_mod.generate())
    ax[3][0].set_title("Frequency modulation")
    
    #
    
    signal_sinusoidal_1 = SignalSinusoidal(domain, amplitude=1, phase = 0, frequency=2.0)
    signal_rec_bip = SignalWaveRectangleUnipolar(domain, amplitude=30, period=3.0, tau=1.5)
    ax[1][1].plot(domain, signal_rec_bip.generate())
    signal_phi_mod = SignalSinusoidalPhaseModulated(domain, signal_sinusoidal_1, signal_rec_bip, k_fi=1.0)
    signal_fre_mod = SignalSinusoidalFrequencyModulated(domain, signal_sinusoidal_1, signal_rec_bip, k_f=1.0)

    ax[0][1].plot(domain, signal_sinusoidal_1.generate())
    ax[2][1].plot(domain, signal_phi_mod.generate())
    ax[3][1].plot(domain, signal_fre_mod.generate())    
    
    fig.tight_layout()
    plt.show()
    
    