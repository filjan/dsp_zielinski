from matplotlib import pyplot as plt
import numpy as np

from ch_1_signals_and_their_parameters.p_1_2_signals_classification.signals_general_model import SignalExponentialDecreasing, SignalSinusoidalExponentiallyDecreasing, SignalSinc, SignalGaussian


if  __name__ == "__main__":

    domain = np.linspace(-6, 6, 1000)
    signal_expo_decr = SignalExponentialDecreasing(domain, amplitude=10, alfa=4)
    signal_sinu_expo_decr = SignalSinusoidalExponentiallyDecreasing(domain, amplitude=10, alfa=4, frequency=10)
    signal_sinc = SignalSinc(domain)
    signal_gaus = SignalGaussian(domain)
    
    fig, ax = plt.subplots(4)
    ax[0].plot(domain, signal_expo_decr.generate())
    ax[1].plot(domain, signal_sinu_expo_decr.generate())
    ax[2].plot(domain, signal_sinc.generate())
    ax[3].plot(domain, signal_gaus.generate())
    [axes.grid() for axes in ax]
    [axes.set_xticks(np.arange(domain[0], domain[-1]+1, 1)) for axes in ax]
    plt.show()    